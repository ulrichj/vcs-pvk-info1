#include <iostream>
#include <cmath>

using namespace std;

void DFT (double x[][2], int N, double X[][2], const double pi);

double input[][2] = {
  {1.0, 2.0},
  {3.0, 4.0},
  {5.0, 6.0}
};

double output[3][2];

int
main ()
{

  int N = sizeof (input) / sizeof (input[0]);

  cout << "INPUT" << endl;
  for (double *z:input)
  {
    cout << z[0] << " + " << z[1] << "i" << endl;
  }
  cout << endl;

  DFT (input, N, output, 3.1415);

  cout << "OUTPUT" << endl;
  for (double *z:output)
  {
    cout << z[0] << " + " << z[1] << "i" << endl;
  }
  return 0;
}

void
DFT (double x[][2], int N, double X[][2], const double pi)
{
  // your code here
  return;
}
