#include <iostream>

using namespace std;

int binarySearch (int n[], int left, int right, int goal);

int
main ()
{
  int values[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
  int N = 9;

  int foundAt = binarySearch (values, 0, N-1, 7);

  if (foundAt != -1)
    cout << "Found at index " << foundAt << endl;
  else
    cout << "Item was not found" << endl;

  return 0;
}

int
binarySearch (int n[], int left, int right, int goal)
{
  // your code here
  return 0;
}
