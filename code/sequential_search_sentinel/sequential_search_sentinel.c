#include <iostream>

using namespace std;

int sequentialSearch (int n[], int N, int goal);

int
main ()
{
  int values[] = { 8, 5, 1, 3, 9, 2, 6, 7, 4, 0 };
  int N = 10;

  int foundAt = sequentialSearch (values, N, 7);

  if (foundAt != N)
    cout << "Found at index " << foundAt << endl;
  else
    cout << "Item was not found" << endl;

  return 0;
}

int
sequentialSearch (int n[], int N, int goal)
{
  n[N] = goal;

  int i = 0;
  while (n[i] != goal)
    i++;

  return i;
}
