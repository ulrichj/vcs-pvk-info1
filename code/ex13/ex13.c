#include <iostream>

using namespace std;

void insertionSort (int n[], int N);

int
main ()
{
  int values[] = { 8, 5, 1, 3, 9, 2, 6, 7, 4 };
  int N = 9;

  insertionSort (values, N);

for (int value:values)
  {
    cout << value << endl;
  }

  return 0;
}

void
insertionSort (int n[], int N)
{
  /* solution */
  // repeat this sorting step as long as there are unsorted elements
  for (int i = 0; i < N; i++)
  {
    // start at the initial position of this element
    int j = i;
    // change with its previous neighbour until the neighbour is smaller
    while (j > 0 && n[j] < n[j - 1])
    {
      // exchange the elements
      int temporary = n[j];
      n[j] = n[j - 1];
      n[j - 1] = temporary;
      // adapt the position of the current element
      j--;
    }
  }
  /* endsolution */
  return;
}
