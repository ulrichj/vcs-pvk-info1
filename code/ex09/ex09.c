#include<iostream>
#include<iomanip>
#include<string>

using namespace std;

void exp(double x[2], double result[2], double eps);

int main() {
  double start = 0;
  double stop = 8;
  double N = 100;
  double step = (stop-start) / N;
  for(double x = start; x < stop; x+=step) {
    double result[] = {0.0, 0.0};
    double input[] = {0, x};
    exp(input, result, 1e-10);
    cout << x << "," << result[0] << "," << result[1] << endl;
  }
  return 0;
}

void exp(double x[2], double result[2], double eps) {
  /* solution */
  double summand[] = {1.0, 0.0};
  double tmp[] = {0.0, 0.0};
  int n = 1;

  while(summand[0]*summand[0] + summand[1]*summand[1] > eps*eps) {
    result[0] += summand[0];
    result[1] += summand[1];
    tmp[0] = summand[0] * x[0] - summand[1] * x[1];  
    tmp[1] = summand[0] * x[1] + summand[1] * x[0];  

    summand[0] = tmp[0] / n;  
    summand[1] = tmp[1] / n;  

    n++;
  }
  /* endsolution */
  return;
}
