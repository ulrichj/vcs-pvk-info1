#!/usr/bin/env python3
import numpy as np
import termplotlib as tpl
import pandas as pd

data = pd.read_csv('output.csv', header=None)

fig = tpl.figure()
fig.plot(data[0], data[1], width=70, height=20)
fig.show()
