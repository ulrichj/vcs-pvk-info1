#include <iostream>

using namespace std;

int FromRussiaWithLove (int m, int n);

int
main ()
{
  cout << " 24 ⋅ 5 = " << FromRussiaWithLove (24, 5) << endl;
  cout << " 25 ⋅ 5 = " << FromRussiaWithLove (25, 5) << endl;
  cout << "-25 ⋅ 6 = " << FromRussiaWithLove (-25, 6) << endl;
  return 0;
}

int
FromRussiaWithLove (int m, int n)
{
  /* solution */
  if (m * m > n * n) {
    int tmp = m;
    m = n;
    n = tmp;
  }
  int p = !(m % 2) ? 0 : n;
  while (m * m > 1) {
    m /= 2;
    n *= 2;
    if (m % 2)
      p += n;
  }
  return m * p;
  /* endsolution */
}
